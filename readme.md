PageModel
=========

PageModel is a refinery engine which enables developer to create a model for a page from the admin interface easily and without having to create a 
separate table for every models, which is normally the condition for any rails model. 


In one of the project I faced this problem where I had to style differently for the different component of the address section for a company. 
The trick was it was meant to be added by the customer, so in the traditional way in RefineryCMS we created a page part where they would put in 
the address i.e. every component like street address, post office, number in the same string. We then managed with that somehow, but this idea 
struck to me that it would be very easy to accomplish that only if we had access to these components separately like address.street,address.post.

So I started this project recently, but it is not complete yet. 
