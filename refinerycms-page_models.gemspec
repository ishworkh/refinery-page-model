# Encoding: UTF-8

Gem::Specification.new do |s|
  s.platform          = Gem::Platform::RUBY
  s.name              = 'refinerycms-page_models'
  s.version           = '1.0'
  s.description       = 'Ruby on Rails Page Models extension for Refinery CMS'
  s.date              = '2014-08-15'
  s.summary           = 'Page Models extension for Refinery CMS'
  s.require_paths     = %w(lib)
  s.files             = Dir["{app,config,db,lib}/**/*"] + ["readme.md"]
  # Runtime dependencies
  s.add_dependency             'refinerycms-core',    '~> 2.1.2'
  s.add_dependency             'refinerycms-i18n'
  
  # Development dependencies (usually used for testing)
  s.add_development_dependency 'refinerycms-testing', '~> 2.1.2'
  s.add_development_dependency 'factory_girl_rails'
end
