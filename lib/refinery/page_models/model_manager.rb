module Refinery
	module PageModels
		class ModelManager
			def initialize(model_collection)
				begin	
					@models=Hash.new
					model_collection.each do |model|
						klass=Refinery::PageModels.const_set(model.name.classify,
							Class.new(BaseModel) do 
								class_variable_set(:@@linked_attributes,model.model_attributes.map{|a| a.attributes})
								class_variable_set(:@@page_model_id,model.id)
								model.model_attributes.each do |attribute|
									name=attribute.name.downcase.parameterize("_").to_sym
									attr_accessor name
								end
							end)
						if klass.class == Class
							if @models.has_key?(model.page_id)
								@models[model.page_id].push(model.name.classify)
							else
								@models[model.page_id]=[model.name.classify]
							end
						end
						
					end

				rescue Exception => e
					@models={}
					puts "Sorry pageModels could not initialize"
				end
			end

			def models
				@models
			end

			def set_relationship 
				yield
			end
		end

		class BaseModel
			include ActiveModel::Validations
			include ActiveModel::Conversion
			extend ActiveModel::Naming

				@@linked_attributes=[]
				@@page_model_id=nil
				@@namespace=::Refinery::PageModels
				
			def initialize(attributes={})
				attributes=get_instance_variables.merge(attributes)
				set_instance_variables(attributes)
			end

			def persisted?
				false
			end

			def update(params={})
				if (params.length > 0)
					formatted_attr=prepare_attr(params)
					if page_model.update_attributes({model_attributes_attributes: formatted_attr})
						set_instance_variables(params)
						linked_attributes=(formatted_attr)
					end
				end
			end

			def linked_attributes=(hash_array=[])
				@@linked_attributes = hash_array
			end
			def linked_attributes
				@@linked_attributes
			end

			def page_model
				if @@page_model_id
					@@namespace::PageModel.find(@@page_model_id)
				end
			end
			private
				def prepare_attr(params={})
					prepared_attr=[]
					params.each do |key,value|
						sel=linked_attributes.select{|at| at['name']==key || at['name'].to_sym == key}
						if sel.count!=0
							sel[0]['value']=value
							prepared_attr.push(sel[0])
						end
					end
					prepared_attr
				end

				def set_instance_variables(params={})
					params.each do |name,value|
						send("#{name}=", value || nil) 
					end
				end

				def get_instance_variables
					instance={}
					@@linked_attributes.each do |record|
						instance[record['name']]=record['value']
					end
					instance
				end
		end
	end
end