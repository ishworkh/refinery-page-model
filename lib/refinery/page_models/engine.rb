module Refinery
  module PageModels
    class Engine < Rails::Engine
      extend Refinery::Engine
      require 'refinery/page_models/model_manager'
      isolate_namespace Refinery::PageModels

      engine_name :refinery_page_models

      before_inclusion do
        Refinery::Plugin.register do |plugin|
          plugin.name = "page_models"
          plugin.url = proc { Refinery::Core::Engine.routes.url_helpers.page_models_admin_page_models_path }
          plugin.pathname = root
          plugin.activity = {
            :class_name => :'refinery/page_models/page_model',
            :title => 'name'
          }
          
        end
      end

      config.after_initialize do
        Refinery.register_extension(Refinery::PageModels)
        modelManager=Refinery::PageModels::ModelManager.new(::Refinery::PageModels::PageModel.all)
        modelManager.set_relationship do 
          Refinery::Page.class_eval do 
            class_variable_set(:@@pageModels,modelManager.models)
            pageModels=class_variable_get(:@@pageModels)
            pageModels.each do |mid,model|
              model.each do |name|
                define_method name.downcase.singularize.parameterize("_") do 
                  Refinery::PageModels.const_get(name.classify).new
                end
              end
            end
           end
        end
      end
    end
  end
end
