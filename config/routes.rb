Refinery::Core::Engine.routes.draw do

  # Frontend routes
  namespace :page_models do
    resources :page_models, :path => '', :only => [:index, :show]
  end

  # Admin routes
  namespace :page_models, :path => '' do
    namespace :admin, :path => Refinery::Core.backend_route do
      resources :page_models, :except => :show do
        collection do
          post :update_positions
        end
      end
    end
  end

end
