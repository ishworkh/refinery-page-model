class CreatePageModelsPageModels < ActiveRecord::Migration

  def up
    create_table :refinery_page_models do |t|
      t.string :name
      t.string :page_name
      t.integer :page_id
      t.integer :position

      t.timestamps
    end

  end

  def down
    if defined?(::Refinery::UserPlugin)
      ::Refinery::UserPlugin.destroy_all({:name => "refinerycms-page_models"})
    end

    if defined?(::Refinery::Page)
      ::Refinery::Page.delete_all({:link_url => "/page_models/page_models"})
    end

    drop_table :refinery_page_models

  end

end
