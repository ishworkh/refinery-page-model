class CreatePageModelsPageModelTranslations < ActiveRecord::Migration
	def up 
		::Refinery::PageModels::PageModel.create_translation_table!(
			{:name=>:string,:page_name=>:string},
				:migrate_data=>true)
		remove_column :refinery_page_models, :name
		remove_column :refinery_page_models, :page_name
	end
	def down 
		add_column :refinery_page_models, :name,:string 
      	add_column :refinery_page_mdoels, :page_name, :string
      	::Refinery::PageModels::PageModel.drop_translation_table! :migrate_data => true 
	end
end