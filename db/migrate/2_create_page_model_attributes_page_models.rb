class CreatePageModelAttributesPageModels < ActiveRecord::Migration
	def up 
		create_table :refinery_page_model_attributes do |t|
			t.string :name
			t.boolean :accessible
			t.string :value
			t.string :data_type
			t.integer :position
			t.integer :page_model_id
			t.timestamps
		end
	end
	def down 
		drop_table :refinery_page_model_attributes
	end
end