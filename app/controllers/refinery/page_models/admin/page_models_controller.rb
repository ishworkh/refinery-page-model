module Refinery
  module PageModels
    module Admin
      class PageModelsController < ::Refinery::AdminController

        crudify :'refinery/page_models/page_model',
                :title_attribute => 'name',
                :xhr_paging => true

      end
    end
  end
end
