module Refinery
  module PageModels
    class PageModelsController < ::ApplicationController
      before_filter :find_all_page_models
      before_filter :find_page

      def index
        # you can use meta fields from your model instead (e.g. browser_title)
        # by swapping @page for @page_model in the line below:
        present(@page)
      end

      def show
        @page_model = PageModel.find(params[:id])

        # you can use meta fields from your model instead (e.g. browser_title)
        # by swapping @page for @page_model in the line below:
        present(@page)
      end

    protected

      def find_all_page_models
        @page_models = PageModel.order('position ASC')
      end

      def find_page
        @page = ::Refinery::Page.where(:link_url => "/page_models").first
      end

    end
  end
end
