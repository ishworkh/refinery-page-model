Refinery::PageModels::Admin::PageModelsController.class_eval do 
	before_filter :get_grouped_instance, only: [:index]

	protected
	def get_grouped_instance
		@grouped_page_models=Refinery::PageModels::PageModel.all.group_by{|p| p.page_name}
	end
end