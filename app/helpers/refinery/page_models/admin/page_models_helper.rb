module Refinery
	module PageModels
		module Admin	
			module PageModelsHelper 
				def add_fields_for(name,f,association)
					new_object=f.object.class.reflect_on_association(association).klass.new
					fields=f.fields_for(association, new_object, child_index: "new_#{association.to_s}") do |fbuilder|
							render "page_#{association.to_s.singularize}_fields", :builder=>fbuilder
						end
					link_to_function name,"add_fields(this, \"#{association}\",\"#{escape_javascript(fields)}\")"
				end
			end
		end
	end
end