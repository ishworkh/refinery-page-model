module Refinery
  module PageModels
    class PageModel < Refinery::Core::BaseModel
      self.table_name = 'refinery_page_models'

      attr_accessible :name, :page_name, :page_id,:model_attributes_attributes, :position

      validates :name, :presence => true, :uniqueness => true
      validates :page_name, :presence => true
      validates :page_id, :presence => true

      has_many :model_attributes,dependent: :destroy
      accepts_nested_attributes_for :model_attributes, :allow_destroy=>true, :reject_if => :all_blank

      translates :name,:page_name

      class Translation
        attr_accessible :locale
      end
    end
  end
end
