module Refinery
  module PageModels
    class ModelAttribute < Refinery::Core::BaseModel
      self.table_name = 'refinery_page_model_attributes'

      attr_accessible :name,:position,:accessible, :value, :data_type, :page_model_id,:updated_at,:created_at

      belongs_to :page_model

      def get_datatypes
        ["boolean","data","datetime","decimal","float","integer","string","text","time","timestamp"]
      end

    end
  end
end
