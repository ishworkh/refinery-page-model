# encoding: utf-8
require "spec_helper"

describe Refinery do
  describe "PageModels" do
    describe "Admin" do
      describe "page_models" do
        refinery_login_with :refinery_user

        describe "page_models list" do
          before do
            FactoryGirl.create(:page_model, :name => "UniqueTitleOne")
            FactoryGirl.create(:page_model, :name => "UniqueTitleTwo")
          end

          it "shows two items" do
            visit refinery.page_models_admin_page_models_path
            page.should have_content("UniqueTitleOne")
            page.should have_content("UniqueTitleTwo")
          end
        end

        describe "create" do
          before do
            visit refinery.page_models_admin_page_models_path

            click_link "Add New Page Model"
          end

          context "valid data" do
            it "should succeed" do
              fill_in "Name", :with => "This is a test of the first string field"
              click_button "Save"

              page.should have_content("'This is a test of the first string field' was successfully added.")
              Refinery::PageModels::PageModel.count.should == 1
            end
          end

          context "invalid data" do
            it "should fail" do
              click_button "Save"

              page.should have_content("Name can't be blank")
              Refinery::PageModels::PageModel.count.should == 0
            end
          end

          context "duplicate" do
            before { FactoryGirl.create(:page_model, :name => "UniqueTitle") }

            it "should fail" do
              visit refinery.page_models_admin_page_models_path

              click_link "Add New Page Model"

              fill_in "Name", :with => "UniqueTitle"
              click_button "Save"

              page.should have_content("There were problems")
              Refinery::PageModels::PageModel.count.should == 1
            end
          end

        end

        describe "edit" do
          before { FactoryGirl.create(:page_model, :name => "A name") }

          it "should succeed" do
            visit refinery.page_models_admin_page_models_path

            within ".actions" do
              click_link "Edit this page model"
            end

            fill_in "Name", :with => "A different name"
            click_button "Save"

            page.should have_content("'A different name' was successfully updated.")
            page.should have_no_content("A name")
          end
        end

        describe "destroy" do
          before { FactoryGirl.create(:page_model, :name => "UniqueTitleOne") }

          it "should succeed" do
            visit refinery.page_models_admin_page_models_path

            click_link "Remove this page model forever"

            page.should have_content("'UniqueTitleOne' was successfully removed.")
            Refinery::PageModels::PageModel.count.should == 0
          end
        end

      end
    end
  end
end
