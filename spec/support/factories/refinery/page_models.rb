
FactoryGirl.define do
  factory :page_model, :class => Refinery::PageModels::PageModel do
    sequence(:name) { |n| "refinery#{n}" }
  end
end

